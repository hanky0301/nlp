package nlg;

import java.lang.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.*;


import javax.json.*;


public class Main {

    public static void main(String[] args) {
        try {
            InputStream is = new FileInputStream("top_5.json");
            JsonReader reader = Json.createReader(is);
            JsonArray collection = reader.readArray();

			List<List<Double>> dep_scores = new ArrayList<>(collection.size());
			BufferedReader br = new BufferedReader(new FileReader("ta_scores.txt"));
			String line;
			for (int i = 0; i < collection.size(); i++) {
				dep_scores.add(new ArrayList<Double>());
				for (int j = 0; j < 5; j++) {
					line = br.readLine();
					dep_scores.get(dep_scores.size() - 1).add(Double.parseDouble(line));
				}
			}
			
			int correct = 0;
			int n_processed = 0;
			System.out.println("Id,Answer");
            for( JsonValue value : collection) {
                Question question = new Question((JsonObject) value);
                question.clean();
				
				List<Double> table = new ArrayList<>();
				for (int i = 14; i >= 0; i--) {
					table.add(Math.pow(1.60, (double)i) / 665);
				}
                List<Double> scores = new ArrayList<>();
                for (int i = 0; i < Question.NUM_OPTIONS; i++) {
					Double score = 0.0;
					for (int j = 0; j < question.context_split.size(); j++) {
						if (question.context_split.get(j).contains(question.options.get(i)))
							score += table.get(j);
					}
					if (question.query_split.contains(question.options.get(i)) || 
						question.options.get(i).contains("cnn"))
						scores.add(-100000.0);
					else
						scores.add((double)score + dep_scores.get(n_processed).get(i));
				}
				//for (int i = 0; i < Question.NUM_OPTIONS; i++)
					//System.out.println(scores.get(i));

                int predict_id = MaxIndex(scores)+ 1; // prediction is 1-based

				System.out.println(String.format("%d,%d",question.id,predict_id) );

				n_processed += 1;
            }

        }catch( IOException e ){
            System.err.println("Cannot open `test-nolabel.json'");
        }

    }

    public static <E extends Comparable > int MaxIndex( List<E> list ){

        assert( list.size() > 0 );

        int max_idx = 0;
        for( int idx = 1; idx < list.size(); idx ++ ) {
            if( list.get(idx).compareTo(list.get(max_idx)) > 0 ) {
                max_idx = idx;
            }
        }

        return max_idx;
    }

}

