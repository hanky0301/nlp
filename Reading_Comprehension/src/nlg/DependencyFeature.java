package nlg;

/**
 * Created by wlzhuang on 2016/5/14.
 */
public class DependencyFeature {


    /* governor -> depedent */

    public String dependent;
    public String link;
    public String governor;
    public String linkComment;

    public DependencyFeature(String D, String L, String G) {
        dependent = D;
        governor = G;
        if (L.contains(":")) {
            String[] arr = L.split(":", 2);
            link = arr[0];
            linkComment = arr[1];
        } else {
            link = L;
            linkComment = "";
        }
    }

    public String toString() {
        if (linkComment.equals("")) {
            return String.format("(%s,%s,%s)", governor, link, dependent);
        } else {
            return String.format("(%s,%s:%s,%s)", governor, link, linkComment, dependent);
        }
    }

    public int hashCode() {
        return dependent.hashCode() + governor.hashCode() + link.hashCode() ;
    }

    @Override
    public boolean equals(Object _feature){
        DependencyFeature feature = (DependencyFeature) _feature;
        if( feature.dependent.equals(dependent)
                && feature.governor.equals(governor)
                && feature.link.equals(link) )
        {
            return true;
        }else{
            return false;
        }
    }

}
