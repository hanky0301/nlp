package nlg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by corsa on 2016/5/14.
 */
public class Range {
    public int start, end;
    public Range( int s, int e){
        start = s;
        end = e;
    }
    public Range(){
        start = -1;
        end = -1;
    }

    public boolean contains(int k){
        if( start <= k && k < end){
            return true;
        }else{
            return false;
        }
    }
    @Override
    public String toString () {
        return "(" + start + "," + end + ")";
    }


    // brute-force search
    public static <E> List<Range> SearchAll(List<E> coll, List<E> p){
        //TODO: use KMP or something faster

        List<Range> ranges = new ArrayList<>();


        for( int i = 0; i < coll.size(); i++ ){
            int j = 0;
            for( j = 0; j < p.size() && i + j < coll.size(); j++) {
                if( ! coll.get(i+j).equals(p.get(j)) ){
                    break;
                }
            }
            if( j == p.size() ){
                ranges.add( new Range(i, i+j) );
            }
        }

        return ranges;
    }
}
