package nlg;

import javax.json.JsonArray;
import javax.json.JsonObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collection;

/**
 * Created by wlzhuang on 2016/5/13.
 */

public class Question {

    static public final int NUM_OPTIONS = 5;
    static public final String PLACEHOLDER = "__BLANK__";

    public int id;
    public String context;
    public String query;
    public List<String> options;
    public int correct_id;
    public int predict_id;

    public List<String> query_split;
    public List<String> context_split;
    public List<List<String>> options_split;
    public int placeholderPosition;

    public Question(JsonObject obj) {
        context = obj.getString("text");
        query   = obj.getString("question");

        JsonArray options_arr = obj.getJsonArray("options");

        options = new ArrayList<>();
        for ( int i = 0; i < NUM_OPTIONS; i++){
            options.add( options_arr.getString(i) );
        }

        id = obj.getInt("id");

        correct_id = obj.getInt("correct");
        predict_id = -1;
        placeholderPosition = -1;
    }

    public void clean() {
        context = context.replaceAll("\\( CNN \\) -- ", "");
        context = context.replaceAll("\\( cnn \\) -- ", "");
        query_split = Arrays.asList( query.split("\\s+") );
        placeholderPosition = query_split.indexOf(PLACEHOLDER);

        context_split = new ArrayList<>();
		int start = 0;
		for (int i = 0; i < context.length(); i++)
		{
			if (context.charAt(i) == '.')
			{
				context_split.add(context.substring(start, i + 1).toLowerCase());
				start = i + 1;
			}
		}

        options_split = new ArrayList<>(NUM_OPTIONS);

        for ( int i = 0; i < NUM_OPTIONS; i++){
            options_split.add(
                Arrays.asList(options.get(i).split("\\s+"))
            );
		//	System.out.print(options_split);
        }
	//	System.out.println();
    }

    public String getSubstitutedOption(int i){
        return query.replaceFirst(PLACEHOLDER, options.get(i) );
    }

    public int getPlaceholderPosition() {
        return placeholderPosition;
    }

    public int getOptionSize(int i) {
        return options_split.get(i).size();
    }
}
