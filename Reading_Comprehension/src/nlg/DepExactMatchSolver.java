package nlg;

import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Properties;

/**
 * Created by wlzhuang on 2016/5/15.
 */
public class DepExactMatchSolver {
    public Question question;

    public Document doc;

    public Properties prop; 

    public DepExactMatchSolver(Question q) {
        assert (q != null);

        question = q;
        doc = new Document(question.context);

        prop = new Properties();
        prop.put("threads", 8);

    }

    public Set<DependencyFeature> findDependenciesOfWords(Set<DependencyFeature> depSet, Sentence sent, List<String> targetWords) {

        if (depSet == null)
            throw new NullPointerException("depSet cannot be null");

        List<Range> ranges = Range.SearchAll(sent.words(), targetWords);

        for (Range range : ranges) {

            for (int p = 0; p < sent.length(); p++) {
                int governor_pos = sent.governor(prop, p).orElse(-1);

                if (governor_pos == -1) {
                    continue;
                }
                String dep = sent.incomingDependencyLabel(prop, p).orElse("");

                if (range.contains(p)) {
                    // target word <- governor
                    if (!range.contains(governor_pos)) {
                        String governor = sent.word(governor_pos);
                        depSet.add(new DependencyFeature("", dep, governor));
                        //System.out.println("\t <= " + governor + "\t" + dep );
                    }
                } else {
                    // simple word <- target(governor)
                    if (range.contains(governor_pos)) {
                        depSet.add(new DependencyFeature(sent.word(p), dep, ""));
                        //System.out.println("\t => " + sent.word(p) + "\t" + dep);
                    }
                }
            }
        }
        return depSet;
    }

    public double getScoreOfOption(int i) {
        String candidate = question.getSubstitutedOption(i);
        Sentence sent = new Document(candidate).sentence(0);

        Set<DependencyFeature> optDeps = new HashSet<>();
        findDependenciesOfWords(optDeps, sent, question.options_split.get(i));

        Set<DependencyFeature> ctxDeps = new HashSet<>();
        for (Sentence sentence : doc.sentences()) {
            findDependenciesOfWords(ctxDeps, sentence, question.options_split.get(i));
        }

        double score = 0;
        for( DependencyFeature dep : optDeps ){
            if( ctxDeps.contains(dep) ){
                score += 1;
            }
        }
        return score;
    }
}
