#! /usr/bin/env python
# encoding=utf-8

import sys
import pickle
import jieba
import numpy as np
from collections import defaultdict


def build_word_dict(filename, label_pkl, word_dict_pkl):
    label = []
    segment_list = []
    word_dict = defaultdict(int)

    with open(filename, 'r') as f:
        for line in f:
            data = line.split("\t")
            label.append(data[1])
            text = data[2].replace("EMOTICON", "").replace("　", "").replace(" ", "").strip("\n")
            segment_list.append(jieba.lcut(text))
            for word in segment_list[-1]:
                word_dict[word] += 1

    word_dict = {word: word_dict[word] for word in word_dict if word_dict[word] > 5}

    with open(word_dict_pkl, 'wb') as f:
        pickle.dump(word_dict, f)

    with open(label_pkl, 'wb') as f:
        pickle.dump(label, f)

    return segment_list, word_dict


def build_word_count(filename, segment_list, word_dict):
    word_count = []
    for segments in segment_list:
        count = defaultdict(int)
        for word in segments:
            if word in word_dict:
                count[word] += 1
        word_count.append(count)

    with open(filename, 'wb') as f:
        pickle.dump(word_count, f)


if __name__ == "__main__":
    segment_list, word_dict = build_word_dict(sys.argv[1], sys.argv[2], sys.argv[3])
    build_word_count(sys.argv[4], segment_list, word_dict)
