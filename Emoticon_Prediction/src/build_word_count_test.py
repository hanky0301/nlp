#! /usr/bin/env python
# encoding=utf-8

import sys
import pickle
import jieba
import numpy as np
from collections import defaultdict


def build_word_dict(filename):
    segment_list = []
    word_dict = defaultdict(int)

    with open(filename, 'r') as f:
        for line in f:
            data = line.split("\t")
            text = data[2].replace("EMOTICON", "").replace("　", "").replace(" ", "").strip("\n")
            segment_list.append(jieba.lcut(text))

    return segment_list


def build_word_count_test(filename, segment_list, word_dict_pkl):
    print word_dict_pkl
    with open(word_dict_pkl, 'rb') as f:
        word_dict = pickle.load(f)
    word_count_test = []

    for segments in segment_list:
        count = defaultdict(int)
        for word in segments:
            if word in word_dict:
                count[word] += 1
        word_count_test.append(count)

    with open(filename, 'wb') as f:
        pickle.dump(word_count_test, f)


if __name__ == "__main__":
    segment_list = build_word_dict(sys.argv[1])
    build_word_count_test(sys.argv[2], segment_list, sys.argv[3])
