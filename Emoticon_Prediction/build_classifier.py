#! /usr/bin/env python
# encoding=utf-8

import numpy as np
import pickle
import sys
from collections import defaultdict
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD, Adagrad

def build_features(label, word_list, word_count, start, end):
    batch_x = []
    batch_y = []
    
    for i in range(start, end):
        feature = np.zeros((32013, ), dtype=np.float32)
        for word in word_count[i]:
            feature[word_list.index(word)] = np.float32(word_count[i][word])
        batch_x.append(feature)
    
        y = np.zeros((40, ), dtype=np.float32)
        y[int(label[i]) - 1] = np.float32(1.0)
        batch_y.append(y)

    batch_x = np.asarray(batch_x)
    batch_y = np.asarray(batch_y)

    return batch_x, batch_y


def train(label, word_list, word_count):
    model = Sequential()
    model.add(Dense(1024, input_dim=32013, init='uniform'))
    model.add(Activation('relu'))
    model.add(Dropout(0.3))
    model.add(Dense(800, init='uniform'))
    model.add(Activation('relu'))
    model.add(Dropout(0.3))
    model.add(Dense(40, init='uniform'))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer=Adagrad())

    '''
    print 'Start training.'
    for i in range(0, 261950, 50):
        batch_x, batch_y = build_features(label, word_list, word_count, i, i + 50)
        model.train_on_batch(batch_x, batch_y)
        if i % 10000 == 0:
            print 'batch', i
    
    model.save_weights('model_weights_1.h5')
    '''
    model.load_weights('model_weights_1.h5')
    return model

def test(model, label, word_list, word_count_test, result_file, model_output):
    print 'start testing'
    #with open(result_file, 'w') as f:
    with open(model_output, 'w') as f2:
        #f.write('Id,Emoticon\n')
        for i in range(0, 68733, 1091):
            print i
            test_x, label_no_use = build_features(label, word_list, word_count_test, i, i + 1091)
            test_y = model.predict(test_x)
            for j in range(i, i + 1091):
                for k in range(40):
                    f2.write(str(test_y[j % 1091][k]) + ' ')
                f2.write('\n')

            '''
            for j in range(i, i + 1091):
                _id = j + 261955
                emo = test_y[j % 1091].argsort()[::-1][:3]
                line = '{},{} {} {}\n'.format(_id, emo[0] + 1, emo[1] + 1, emo[2] + 1)
                f.write(line)
            '''

if __name__ == "__main__":
    with open(sys.argv[1], 'rb') as f:
        label = pickle.load(f)

    with open(sys.argv[2], 'rb') as f:
        word_dict = pickle.load(f)
    
    with open(sys.argv[3], 'rb') as f:
        word_count = pickle.load(f)

    with open(sys.argv[4], 'rb') as f:
        word_count_test = pickle.load(f)

    word_list = word_dict.keys()
    print len(word_list)

    model = train(label, word_list, word_count)
    test(model, label, word_list, word_count_test, sys.argv[5], sys.argv[6])
