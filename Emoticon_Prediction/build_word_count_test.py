#! /usr/bin/env python
# encoding=utf-8

import sys
import pickle
import jieba
import numpy as np
from collections import defaultdict


def segmentation(filename):
    segment_list = []
    unigram_list = []

    with open(filename, 'r') as f:
        for line in f:
            data = line.split("\t")
            text = data[2].replace("EMOTICON", "").replace("　", "").replace(" ", "").strip("\n")

            segment_list.append(jieba.lcut(text))

            unigrams = []
            for c in unicode(text, "utf-8"):
                if not c.encode("utf-8").isalpha():
                    unigrams.append(c)
            unigram_list.append(unigrams)

    return segment_list, unigram_list


def build_word_count_test(filename, segment_list, unigram_list, word_dict_pkl):
    print 'Using', word_dict_pkl
    with open(word_dict_pkl, 'rb') as f:
        word_dict = pickle.load(f)

    word_count_test = []
    for segments,unigrams in zip(segment_list, unigram_list):
        count = defaultdict(int)
        for word in segments:
            if word in word_dict:
                count[word] += 1
        for unigram in unigrams:
            if unigram in word_dict:
                count[unigram] += 1
        word_count_test.append(count)

    with open(filename, 'wb') as f:
        pickle.dump(word_count_test, f)


if __name__ == "__main__":
    segment_list, unigram_list = segmentation(sys.argv[1])
    build_word_count_test(sys.argv[2], segment_list, unigram_list, sys.argv[3])
